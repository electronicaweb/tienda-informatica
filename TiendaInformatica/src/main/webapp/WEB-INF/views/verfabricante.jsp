<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="style.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Fabricante</title>
</head>
<body>
	<table>
		<tr>
			<th>Codigo</th>
			<th>Nombre</th>
			<th>Apellidos</th>
			<th>Telefono</th>
		</tr>
		<tr>
				<th id="fabricante"><c:out value="${fabricante.getCodigo_fabricante()}"></c:out></th>
				<th id="fabricante"><c:out value="${fabricante.getNombre_fabricante()}"></c:out></th>
				<th id="fabricante"><c:out value="${fabricante.getApellidos_fabricante()}"></c:out></th>
				<th id="fabricante"><c:out value="${fabricante.getTelefono_fabricante()}"></c:out></th>
				<th id="fabricante"></th>
				<th id="fabricante"><a href="/electronicaweb/eliminar/fabricante/${fabricante.getCodigo_fabricante()}"><button id="borrar">Eliminar</button></a>
									<a href="/electronicaweb/editar/fabricante/${fabricante.getCodigo_fabricante()}"><button id="editar">Editar</button></a>
		</tr>
		 	<c:forEach items="${fabricante.getArticulos()}" var="item2">
		 		<tr>
		 		<td></td>
		 		<td><c:out value="${item2.getCodigo_articulo()}"></c:out></td>
					<td><c:out value="${item2.getNombre_articulo()}"></c:out></td>
					<td><c:out value="${item2.getPrecio()}"></c:out></td>
					<td><c:out value="${item2.getIdprueba()}"></c:out></td>
					<td><a href="/electronicaweb/verarticulo/${item2.getIdprueba()}"><button id="ver">Ver</button></a></td>
				</tr>
			</c:forEach>
		</table>
	<div>
	<%@ include file="botones.jsp" %>
	</div>
</body>
</html>