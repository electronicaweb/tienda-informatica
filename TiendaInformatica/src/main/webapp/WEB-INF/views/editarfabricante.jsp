<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="style.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>A�adir articulo</title>
</head>
<body>
	<div>
		<form action="/electronicaweb/modificarfabricante" method="POST">
			<label for="nombre_fabricante">Nombre fabricante:</label>
			<input type="text" name="nombre_fabricante" value="${fabricante.getNombre_fabricante()}" placeholder="${fabricante.getNombre_fabricante()}">
			
			<label for="apellidos_fabricante">Apellido fabricante:</label>
			<input type="text" name="apellidos_fabricante" value="${fabricante.getApellidos_fabricante()}" placeholder="${fabricante.getApellidos_fabricante()}">
			
			<label for="telefono_fabricante">Tel�fono fabricante:</label>
			<input type="text" name="telefono_fabricante" value="${fabricante.getTelefono_fabricante()}" placeholder="${fabricante.getTelefono_fabricante()}">
			<input type="hidden" name="codigo_fabricante" value="${fabricante.getCodigo_fabricante()}">
			<input type="submit" class="button" id="editar"></input>
		</form>
	</div>
	<div>
	<%@ include file="botones.jsp" %>
	</div>
</body>
</html>

