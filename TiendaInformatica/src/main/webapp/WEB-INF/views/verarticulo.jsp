<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="style.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Articulo</title>
</head>
<body>
	<table>
		<tr>
			<th>Codigo Articulo</th>
			<th>Nombre Articulo</th>
			<th>Precio</th>
			<th>Fabricante</th>
		<th>ID</th>
		</tr>
		<tr>
				<td><c:out value="${articulo.getCodigo_articulo()}"></c:out></td>
				<td><c:out value="${articulo.getNombre_articulo()}"></c:out></td>
				<td><c:out value="${articulo.getPrecio()}"></c:out></td>
				<td><c:out value="${articulo.getFabricante()}"></c:out></td>
				<td><c:out value="${articulo.getIdprueba()}"></c:out></td>
				<td><a href="/electronicaweb/eliminar/articulo/${articulo.getIdprueba()}"><button id="borrar">Eliminar</button></a>
					<a href="/electronicaweb/editar/articulo/${articulo.getIdprueba()}"><button id="editar">Editar</button></a></td>
		</tr>
	</table>
	<div>
	<%@include file="botones.jsp" %>
	</div>
</body>
</html>