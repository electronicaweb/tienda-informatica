<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="style.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Editar articulo</title>
</head>
<body>
	<div>
		<form action="/electronicaweb/modificararticulo" method="POST">
			<label for="codigo_articulo">Codigo Articulo:</label>
			<input type="text" name="codigo_articulo" value="${articulo.getCodigo_articulo()}" placeholder="${articulo.getCodigo_articulo()}">
			
			<label for="cnombre_articulo">Nombre Articulo:</label>
			<input type="text" name="nombre_articulo" value="${articulo.getNombre_articulo()}" placeholder="${articulo.getNombre_articulo()}">
			
			<label for="precio">Precio:</label>
			<input type="text" name="precio" value="${articulo.getPrecio()}" placeholder="${articulo.getPrecio()}">
			
			<label for="fabricante">Fabricante:</label>
			<input type="text" name="fabricante" value="${articulo.getFabricante()}" placeholder="${articulo.getFabricante()}">
			<input type="hidden" name="idprueba" value="${articulo.getIdprueba()}">
			<input type="submit" class="button" id="editar">
		</form>
	</div>
	<div>
	<%@ include file="botones.jsp" %>
	</div>
</body>
</html>