<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="style.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Aņadir articulo</title>
</head>
<body>
<div>
	<form action="/electronicaweb/aņadirarticulo" method="POST">
		<label for="codigo_articulo">Codigo Articulo:</label>
		<input type="text" name="codigo_articulo" placeholder="Art000">
		
		<label for="cnombre_articulo">Nombre Articulo:</label>
		<input type="text" name="nombre_articulo" placeholder="Nombre Articulo...">
		
		<label for="precio">Precio:</label>
		<input type="text" name="precio" placeholder="99.99">
		
		<label for="fabricante">Fabricante:</label>
		<input type="text" name="fabricante" placeholder="0">
		<input type="submit" class="button"></input>
	</form>
</div>
<div>
<%@ include file="botones.jsp" %>
</div>
</body>
</html>