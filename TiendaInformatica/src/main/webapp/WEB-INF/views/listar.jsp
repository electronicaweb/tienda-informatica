<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="style.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Lista <c:out value="${tipo}"></c:out></title>
</head>
<body>
<div>
<%@ include file="botones.jsp" %>  <a href="/electronicaweb/a�adir/${tipo}"><button>A�adir</button></a></div>
<table>
<c:set var = "tipo" value = "${tipo}"/>
<c:if test="${fn:contains(tipo, 'articulo')}">
	<tr>
		<th>Codigo Articulo</th>
		<th>Nombre Articulo</th>
		<th>Precio</th>
		<th>Fabricante</th>
		<th>ID</th>
	</tr>
	<c:forEach items="${lista}" var="item">		
			<tr>
				<td><c:out value="${item.getCodigo_articulo()}"></c:out></td>
				<td><c:out value="${item.getNombre_articulo()}"></c:out></td>
				<td><c:out value="${item.getPrecio()}"></c:out></td>
				<td><c:out value="${item.getFabricante()}"></c:out></td>
				<td><c:out value="${item.getIdprueba()}"></c:out></td>
				<td><a href="/electronicaweb/ver${tipo}/${item.getIdprueba()}"><button id="ver">Ver</button></a>
					<a href="/electronicaweb/eliminar/${tipo}/${item.getIdprueba()}"><button id="borrar">Eliminar</button></a>
					<a href="/electronicaweb/editar/${tipo}/${item.getIdprueba()}"><button id="editar">Editar</button></a></td>
			</tr>   
	</c:forEach>
</c:if>
<c:if test="${fn:contains(tipo, 'fabricante')}">
	<tr>
		<th>Codigo</th>
		<th>Nombre</th>
		<th>Apellidos</th>
		<th>Telefono</th>
	</tr>
	<c:forEach items="${lista}" var="item">
		<tr>
			<th id="fabricante"><c:out value="${item.getCodigo_fabricante()}"></c:out></th>
			<th id="fabricante"><c:out value="${item.getNombre_fabricante()}"></c:out></th>
			<th id="fabricante"><c:out value="${item.getApellidos_fabricante()}"></c:out></th>
			<th id="fabricante"><c:out value="${item.getTelefono_fabricante()}"></c:out></th>
			<th id="fabricante"></th>
			<th id="fabricante"><a href="/electronicaweb/ver${tipo}/${item.getCodigo_fabricante()}"><button id="ver">Ver</button></a>    
				<a href="/electronicaweb/eliminar/${tipo}/${item.getCodigo_fabricante()}"><button id="borrar">Eliminar</button></a>
				<a href="/electronicaweb/editar/${tipo}/${item.getCodigo_fabricante()}"><button id="editar">Editar</button></a>
			</tr>
	 	<c:forEach items="${item.getArticulos()}" var="item2">
	 		<tr>
	 		<td></td>
	 		<td><c:out value="${item2.getCodigo_articulo()}"></c:out></td>
				<td><c:out value="${item2.getNombre_articulo()}"></c:out></td>
				<td><c:out value="${item2.getPrecio()}"></c:out></td>
				<td><c:out value="${item2.getIdprueba()}"></c:out></td>
			</tr>
		</c:forEach>
		
	</c:forEach>
</c:if>
</table>

</body>
</html>