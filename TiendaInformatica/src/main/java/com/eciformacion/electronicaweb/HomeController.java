package com.eciformacion.electronicaweb;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.eciformacion.electronicaweb.persistencia.GestorDatos;
import com.eciformacion.electronicaweb.persistencia.pojos.Articulo;
import com.eciformacion.electronicaweb.persistencia.pojos.Fabricante;


@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
	private GestorDatos gestorDatos;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	
	@GetMapping("/listar/{tipo}")
	public String listar(@PathVariable("tipo")String tipo, Model model) {
		
		model.addAttribute("tipo", tipo);

		if (tipo.equals("articulo")) {
			model.addAttribute("lista", gestorDatos.getArticuloMapper().getArticulos());
		}else if (tipo.equals("fabricante")) {
			model.addAttribute("lista", gestorDatos.getFabricanteMapper().getFabricantes());
		}
		
		return "listar";
	}
	
	@GetMapping("/verarticulo/{idprueba}")
	public String verArticulo(@PathVariable("idprueba")int idprueba, Model model) {
		
		model.addAttribute("articulo", gestorDatos.getArticuloMapper().getArticulo(idprueba));
		
		return "verarticulo";
	}
	
	@GetMapping("/verfabricante/{codigo_fabricante}")
	public String verFabricante(@PathVariable("codigo_fabricante")int codigo_fabricante, Model model) {
		
		model.addAttribute("fabricante", gestorDatos.getFabricanteMapper().getFabricante(codigo_fabricante));
		
		return "verfabricante";
	}
	
	@GetMapping("/añadir/{tipo}")
	public String insertar(@PathVariable("tipo")String tipo,
						Model model) {
				
		
		return "añadir" + tipo;
	}
	
	@ModelAttribute("añadirarticulo")
	public Articulo addArticulo(@RequestParam(value="codigo_articulo", required=false)String codigo_articulo,
			@RequestParam(value="nombre_articulo", required=false )String nombre_articulo,
			@RequestParam(value="precio", required=false)String precio,
			@RequestParam(value="fabricante", required=false)String fabricante,@RequestParam(value="idprueba", required=false) String idprueba) {
				
		try{
			Articulo articulo = new Articulo();
			if(idprueba!=null) articulo.setIdprueba(Integer.parseInt(idprueba));
			articulo.setCodigo_articulo(codigo_articulo);
			articulo.setNombre_articulo(nombre_articulo);
			articulo.setPrecio(Float.parseFloat(precio));
			articulo.setFabricante(Integer.parseInt(fabricante));
			return articulo;
			
		}catch(Exception e){
			return null;
		}
	}
		
	
	@PostMapping("/añadirarticulo")
	public String insertarArticulo(@ModelAttribute("añadirarticulo")Articulo articulo,
				Model model) {

		model.addAttribute("articulo", gestorDatos.getArticuloMapper().añadirArticulo(articulo));
		
		return "home";
	}
	
	@ModelAttribute("añadirfabricante")
	public Fabricante addFabricante(@RequestParam(value="nombre_fabricante", required=false )String nombre_fabricante,
			@RequestParam(value="apellidos_fabricante", required=false)String apellidos_fabricante,
			@RequestParam(value="telefono_fabricante", required=false)String telefono_fabricante, 
			@RequestParam(value="codigo_fabricante", required=false)String codigo_fabricante) {
				
		try{
			Fabricante fabricante = new Fabricante();
			if(codigo_fabricante!=null) fabricante.setCodigo_fabricante(Integer.parseInt(codigo_fabricante));
			fabricante.setNombre_fabricante(nombre_fabricante);
			fabricante.setApellidos_fabricante(apellidos_fabricante);
			fabricante.setTelefono_fabricante(telefono_fabricante);
			return fabricante;
			
		}catch(Exception e){
			return null;
		}
	}
	
	@PostMapping("/añadirfabricante")
	public String insertarFabricante(@ModelAttribute("añadirfabricante")Fabricante fabricante,
					Model model) {
		
		model.addAttribute("fabricante", gestorDatos.getFabricanteMapper().añadirFabricante(fabricante));
		
		return "home";
	}
	
	@GetMapping("/eliminar/articulo/{idprueba}")
	public String eliminarArticulo(@PathVariable("idprueba") int idprueba,Model model) {
		model.addAttribute("articulo", gestorDatos.getArticuloMapper().eliminarArticulo(idprueba));
		return "home";
	}
	
	@GetMapping("/eliminar/fabricante/{codigo_fabricante}")
	public String eliminarFabricante(@PathVariable("codigo_fabricante") int codigo_fabricante, 
			Model model) {
		
		model.addAttribute("fabricante", gestorDatos.getFabricanteMapper().eliminarFabricante(codigo_fabricante));
		
		return "home";
	}
	
	@PostMapping("/modificararticulo")
	public String modificarArticulo(@ModelAttribute("añadirarticulo")Articulo articulo,
				Model model) {

		model.addAttribute("articulo", gestorDatos.getArticuloMapper().modificarArticulo(articulo));
		
		return "home";
	}
	
	@PostMapping("/modificarfabricante")
	public String modificarFabricante(@ModelAttribute("añadirfabricante")Fabricante fabricante,
					Model model) {
		
		model.addAttribute("fabricante", gestorDatos.getFabricanteMapper().modificarFabricante(fabricante));
		
		return "home";
	}
	@GetMapping("/editar/{tipo}/{id}")
	public String editar(@PathVariable("tipo")String tipo,@PathVariable("id")String id,
						Model model) {
				if(tipo.contains("articulo")) {
					model.addAttribute("articulo", gestorDatos.getArticuloMapper().getArticulo(Integer.parseInt(id)));
				}else if(tipo.contains("fabricante")) {
					model.addAttribute("fabricante", gestorDatos.getFabricanteMapper().getFabricante(Integer.parseInt(id)));
				}
		
		return "editar" + tipo;
	}
}
