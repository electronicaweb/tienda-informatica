package com.eciformacion.electronicaweb;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.eciformacion.electronicaweb.persistencia.GestorDatos;
import com.eciformacion.electronicaweb.persistencia.pojos.Articulo;

@RestController
@RequestMapping("/articulo")
public class ApiArticulo {
	@Autowired
	private GestorDatos gestorDatos;

	public final GestorDatos getGestorDatos() {
		return gestorDatos;
	}

	public final void setGestorDatos(GestorDatos gestorDatos) {
		this.gestorDatos = gestorDatos;
	}
	
	@ModelAttribute("articulo")
	public Articulo addArticulo(@PathVariable(value="codigo_articulo", required=false)String codigo_articulo,
			@PathVariable(value="nombre_articulo", required=false )String nombre_articulo,
			@PathVariable(value="precio", required=false)String precio,
			@PathVariable(value="fabricante", required=false)String fabricante,
			@PathVariable(value="id", required=false) String idprueba) {
				
		try{
			Articulo articulo = new Articulo();
			if(idprueba!=null) articulo.setIdprueba(Integer.parseInt(idprueba));
			articulo.setCodigo_articulo(codigo_articulo);
			articulo.setNombre_articulo(nombre_articulo);
			articulo.setPrecio(Float.parseFloat(precio));
			articulo.setFabricante(Integer.parseInt(fabricante));
			return articulo;
			
		}catch(Exception e){
			return null;
		}
	}
	@GetMapping("/")
	@PostMapping("/")
	public List getArticulos() {
		return this.gestorDatos.getArticuloMapper().getArticulos();
	}
	
	@GetMapping("/{id}")
	public Articulo getArticulo(@PathVariable("id")int id) {
		return this.gestorDatos.getArticuloMapper().getArticulo(id);
	}
	@GetMapping("/{codigo_articulo}/{nombre_articulo}/{precio}/{fabricante}")
	public Articulo addArticulo(@ModelAttribute("articulo") Articulo art) {
		this.gestorDatos.getArticuloMapper().aņadirArticulo(art);
		return art;
	}
	@GetMapping("/{id}/{codigo_articulo}/{nombre_articulo}/{precio}/{fabricante}")
	public Articulo modArticulo(@ModelAttribute("articulo") Articulo art) {
		this.gestorDatos.getArticuloMapper().modificarArticulo(art);
		return art;
	}
	@GetMapping("/{id}/borrar")
	public int borrarArticulo(@PathVariable("id")int id) {
		return this.gestorDatos.getArticuloMapper().eliminarArticulo(id);
	}

}
