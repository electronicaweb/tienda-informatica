package com.eciformacion.electronicaweb.persistencia.pojos;

import java.io.Serializable;

public class Articulo implements Serializable{
	private String codigo_articulo;
	private String nombre_articulo;
	private double precio;
	private int fabricante;
	private int idprueba;
	public final String getCodigo_articulo() {
		return codigo_articulo;
	}
	public final void setCodigo_articulo(String codigo_articulo) {
		this.codigo_articulo = codigo_articulo;
	}
	public final String getNombre_articulo() {
		return nombre_articulo;
	}
	public final void setNombre_articulo(String nombre_artuculo) {
		this.nombre_articulo = nombre_artuculo;
	}
	public final double getPrecio() {
		return precio;
	}
	public final void setPrecio(double precio) {
		this.precio = precio;
	}
	public final int getFabricante() {
		return fabricante;
	}
	public final void setFabricante(int fabricante) {
		this.fabricante = fabricante;
	}
	public final int getIdprueba() {
		return idprueba;
	}
	public final void setIdprueba(int idprueba) {
		this.idprueba = idprueba;
	}
}
