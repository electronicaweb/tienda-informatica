package com.eciformacion.electronicaweb.persistencia;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.eciformacion.electronicaweb.persistencia.mappers.ArticuloMapper;
import com.eciformacion.electronicaweb.persistencia.mappers.FabricanteMapper;

@Component
public class GestorDatos {
	@Autowired
	private ArticuloMapper articuloMapper;
	@Autowired
	private FabricanteMapper fabricanteMapper;
	
	public ArticuloMapper getArticuloMapper() {
		return articuloMapper;
	}
	
	public void setArticuloMapper(ArticuloMapper articuloMapper) {
		this.articuloMapper = articuloMapper;
	}
	
	public FabricanteMapper getFabricanteMapper() {
		return fabricanteMapper;
	}
	
	public void setFabricanteMapper(FabricanteMapper fabricanteMapper) {
		this.fabricanteMapper = fabricanteMapper;
	}

}
