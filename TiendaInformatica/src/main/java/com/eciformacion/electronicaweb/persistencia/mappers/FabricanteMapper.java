package com.eciformacion.electronicaweb.persistencia.mappers;

import java.util.List;

import com.eciformacion.electronicaweb.persistencia.pojos.Fabricante;

public interface FabricanteMapper {
	
	public List<Fabricante> getFabricantes();
	
	public Fabricante getFabricante(int codigo_fabricante);
	
	public int añadirFabricante(Fabricante fabricante);
	
	public int modificarFabricante(Fabricante fabricante);
	
	public int eliminarFabricante(int codigo_fabricante);
}
