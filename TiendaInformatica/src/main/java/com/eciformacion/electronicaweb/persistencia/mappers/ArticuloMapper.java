package com.eciformacion.electronicaweb.persistencia.mappers;

import java.util.List;

import com.eciformacion.electronicaweb.persistencia.pojos.Articulo;
public interface ArticuloMapper {
	
	public List<Articulo> getArticulos();
	
	public  Articulo getArticulo(int idprueba);
	
	public int aņadirArticulo(Articulo articulo);
	
	public int modificarArticulo(Articulo articulo);
	
	public int eliminarArticulo(int idprueba);
}
