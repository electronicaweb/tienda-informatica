package com.eciformacion.electronicaweb;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eciformacion.electronicaweb.persistencia.GestorDatos;

@RestController
@RequestMapping("/fabricante")
public class ApiFabricante {
	@Autowired
	private GestorDatos gestorDatos;

	public final GestorDatos getGestorDatos() {
		return gestorDatos;
	}

	public final void setGestorDatos(GestorDatos gestorDatos) {
		this.gestorDatos = gestorDatos;
	}
	
	@GetMapping("/")
	@PostMapping("/")
	public List getFabricante() {
		return this.gestorDatos.getFabricanteMapper().getFabricantes();
	}
}
